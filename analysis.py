"""Tools for analysis of output. Currently only contains a tool to make a
plot of convergence.

This can be imported or run directly as a module as

    python -m neural.analysis <file-to-analyze>
"""

import numpy as np


class ConvergencePlot:
    """Creates plot analyzing the convergence behavior.
    Only works on BPNeural log files at the moment."""

    def __init__(self, logfile):
        
        f = open(logfile, 'r')
        lines = f.read().splitlines()
        f.close()

        # Get number of images.
        for line in lines:
            if 'unique images after hashing.' in line:
                no_images = float(line.split()[0])
                break

        # Find where convergence data starts.
        startline = None
        for index, line in enumerate(lines):
            if 'Starting optimization of cost function...' in line:
                startline = index
                break

        # Get parameters.
        ready = [False, False, False, False]
        for index, line in enumerate(lines[startline:]):
            if 'Energy goal' in line:
                ready[0] = True
                energygoal = float(line.split()[-1])
            elif 'Force goal' in line:
                ready[1] = True
                forcegoal = float(line.split()[-1])
            elif 'force coefficient' in line:
                ready[2] = True
                forcecoefficient = float(line.split()[-1])
            elif 'No force training.' in line:
                ready[1] = True
                ready[2] = True
                forcegoal = None
                forcecoefficient = None
            elif line.split()[0] == '0':
                ready[3] = True
                startline = startline + index
            if ready == [True, True, True, True]:
                break
            
        if forcegoal:
            E = energygoal**2 * no_images
            F = energygoal**2 * no_images
            costfxngoal = E + forcecoefficient * F
        else:
            costfxngoal = energygoal**2 * no_images

        # Extract data.
        steps, es, fs, costfxns = [], [], [], []
        costfxnEs, costfxnFs = [], []
        for line in lines[startline:]:
            if 'Saving checkpoint' in line:
                continue
            elif 'onvergence!' in line:
                continue
            elif 'unconverged!' in line:
                continue
            elif 'uncoverged!' in line:
                continue
            elif 'optimization completed successfully.' in line:
                break
            elif 'could not find parameters for the' in line:
                break
            if forcegoal:
                print(line)
                step, time, costfxn, e, f = line.split()
                fs.append(float(f))
            else:
                step, time, costfxn, e = line.split()
            steps.append(int(step))
            es.append(float(e))
            costfxns.append(costfxn)

            # Determine components of the cost function.
            if forcegoal:
                E = float(e)**2 * no_images
                F = float(f)**2 * no_images
                costfxnEs.append(E / float(costfxn))
                costfxnFs.append(forcecoefficient * F / float(costfxn))

        # Make plots.

        from matplotlib import pyplot

        fig = pyplot.figure(figsize=(6., 8.))
        # Margins, vertical gap, and top-to-bottom ratio of figure.
        lm, rm, bm, tm, vg, tb = 0.12, 0.05, 0.08, 0.03, 0.08, 4.
        bottomaxheight = (1. - bm - tm - vg) / (tb + 1.)

        ax = fig.add_axes((lm, bm + bottomaxheight + vg,
                           1. - lm - rm, tb * bottomaxheight))
        ax.semilogy(steps, es, 'b', lw=2, label='energy')
        if forcegoal:
            ax.semilogy(steps, fs, 'g', lw=2, label='forces')
        ax.semilogy(steps, costfxns, color='0.5', lw=2, label='costfxn')
        # Targets.
        ax.semilogy([steps[0], steps[-1]], [energygoal] * 2, color='b',
                    linestyle=':')
        if forcegoal:
            ax.semilogy([steps[0], steps[-1]], [forcegoal] * 2, color='g',
                        linestyle=':')
        ax.semilogy([steps[0], steps[-1]], [costfxngoal] * 2, color='0.5',
                    linestyle=':')
        ax.set_ylabel('RMSE')
        ax.legend()

        if forcegoal:
            ax = fig.add_axes((lm, bm, 1. - lm - rm, bottomaxheight))
            ax.fill_between(x=np.array(steps), y1=costfxnEs, color='blue')
            ax.fill_between(x=np.array(steps), y1=costfxnEs,
                            y2=np.array(costfxnEs) + np.array(costfxnFs),
                            color='green')
            ax.set_ylabel('costfxn component')
            ax.set_xlabel('step')
            ax.set_ylim(0, 1)

        self._fig = fig

    def getfig(self):
        """Returns the figure object."""
        return self._fig

    def savefig(self, filename='convergence.pdf'):
        """Saves the figure object to filename."""
        self._fig.savefig(filename)

        
def plot_convergence(logfile, plotfile='convergence.pdf'):
    """Makes a plot of the convergence of the cost function and its energy
    and force components. Currently only implemented for the BPNeural
    calculator with standard parameters."""
    plot = ConvergencePlot(logfile)
    plot.savefig(plotfile)


if __name__ == '__main__':
    import sys
    assert len(sys.argv) == 2

    logfile = sys.argv[-1]

    plot_convergence(logfile=logfile)
