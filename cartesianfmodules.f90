!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      ! Fortran Version = 2
      subroutine check_version(version, warning) ! -- checked
        implicit none
        integer :: version, warning
!f2py         intent(in) :: version
!f2py         intent(out) :: warning
        if (version .NE. 2) then
          warning = 1
        else
          warning = 0
        end if     
      end subroutine check_version
       
 
  
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine process_o(o_input, weight_no_row, weight_no_col,&
      weight, activation_function, o_output) ! -- checked

        implicit none
        integer :: weight_no_row, weight_no_col
        double precision, dimension (1, weight_no_row - 1) :: o_input
        double precision, dimension (weight_no_row, weight_no_col) ::&
        weight
        character(len=7) :: activation_function  
        double precision, dimension (1, weight_no_col) :: o_output
!f2py   intent (in) :: o_input    
!f2py   intent (in) :: weight_no_row
!f2py   intent (in) :: weight_no_col
!f2py  intent (in) :: weight
!f2py  intent (in) :: activation_function
!f2py  intent (out) :: o_output
        double precision, dimension (weight_no_row) :: ohat
        double precision, dimension (weight_no_col) :: net
        integer :: i, j

         
        ohat = 0.0d0
        do i = 1, weight_no_row - 1
          ohat(i) = o_input(1, i)
        enddo
        ohat(weight_no_row) = 1.0d0  ! one bias
        net = 0.0d0
        o_output = 0.0d0
        do i = 1, weight_no_col
          do j = 1, weight_no_row
            net(i) = net(i) + ohat(j) * weight(j,i)
          enddo
          if (activation_function .NE. 'linear' .AND.& 
          activation_function .NE. 'tanh' .AND.&
          activation_function .NE. 'sigmoid') then
            print *, "Fortran Error: Unknown activation function!"
          else if (activation_function .EQ. 'linear') then
            o_output(1, i) = net(i)
          else if (activation_function .EQ. 'tanh') then
            o_output(1, i) = tanh(net(i))
          else if (activation_function .EQ. 'sigmoid') then    
            o_output(1, i) = 1./(1. + exp(-net(i)))
          end if
        enddo
    
      end subroutine process_o
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      subroutine calculate_error(no_atom, nn_values, real_values,& 
                 no_values, rmse_per_atom, square_error) ! -- checked

        implicit none
        double precision :: rmse_per_atom, square_error
        integer :: no_atom, no_values
        double precision, dimension (no_values) :: nn_values, real_values       
!f2py intent (in) :: no_atom, no_values, nn_values, real_values
!f2py intent (out) :: rmse_per_atom, square_error
        integer :: i
        double precision :: square_error_per_image

        square_error = 0.0d0
        do i = 1, no_values
          square_error = square_error + &
          (nn_values(i) - real_values(i)) ** (2.0d0)
        end do
        square_error_per_image = square_error / no_values
        rmse_per_atom = sqrt(square_error_per_image) / no_atom
      end subroutine calculate_error
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      module images_data ! -- checked

        double precision :: energy_coefficient
        double precision :: force_coefficient
        integer, allocatable :: no_nodes_of_layers(:)
        integer :: no_layers_of_nn
        integer :: len_of_no_nodes_of_layers ! might be superfluous
        integer :: no_all_nodes_of_layers
        integer :: no_of_images
        double precision, allocatable :: real_energies(:)
        double precision, allocatable :: real_forces(:, :)
        integer, allocatable :: no_of_atoms_of_images(:)
        logical :: train_forces
        integer :: no_procs
        integer, allocatable :: no_sub_images(:)
        double precision, allocatable :: input_positions_of_images(:, :)


      end module images_data

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    subroutine feedforward_backpropagation(proc,&
        len_of_parameters, parameters, &
        activation, square_error_energy, &
        square_error_forces, der_parameters_square_error) ! -- Checked
              
      use images_data                        
      implicit none

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! input/output variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      character(len=7) :: activation
      integer :: len_of_parameters, proc
      double precision :: parameters(len_of_parameters)
      double precision :: square_error_energy, square_error_forces
      double precision :: der_parameters_square_error(len_of_parameters)
!f2py         intent(in) :: parameters , len_of_parameters, activation, proc
!f2py         intent(out) :: square_error_energy, square_error_forces
!f2py         intent(out) :: der_parameters_square_error
!f2py         intent(hidden) :: o, D, ohat, nn_energy

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! type definition !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      type :: real_one_d_array_type
        sequence
        double precision, allocatable :: onedarray(:)
      end type real_one_d_array_type

      type :: integer_one_d_array_type
        sequence
        integer, allocatable :: onedarray(:)
      end type integer_one_d_array_type

      type :: real_two_d_array_type
        sequence
        double precision, allocatable :: twodarray(:,:)
      end type real_two_d_array_type

      type :: integer_two_d_array_type
        sequence
        integer, allocatable :: twodarray(:,:)
      end type integer_two_d_array_type

      type :: image_forces_type
        sequence
        double precision, allocatable :: atomic_forces(:, :) 
      end type image_forces_type

      type :: embedded_real_one_two_d_array_type
        sequence
        type(real_two_d_array_type), allocatable :: onedarray(:)
      end type embedded_real_one_two_d_array_type

      type :: embedded_integer_one_two_d_array_type
        sequence
        type(integer_two_d_array_type), allocatable :: onedarray(:)
      end type embedded_integer_one_two_d_array_type

      type :: embedded_real_one_one_d_array_type
        sequence
        type(real_one_d_array_type), allocatable :: onedarray(:)
      end type embedded_real_one_one_d_array_type

      type :: embedded_integer_one_one_d_array_type
        sequence
        type(integer_one_d_array_type), allocatable :: onedarray(:)
      end type embedded_integer_one_one_d_array_type

      type :: embedded_one_one_two_d_array_type
        sequence
        type(embedded_real_one_two_d_array_type), allocatable :: &
        onedarray(:)
      end type embedded_one_one_two_d_array_type

      type :: element_parameters_type
        sequence
        double precision :: scaling_intercept            
        double precision :: scaling_slope
        type(real_two_d_array_type), allocatable :: weights(:)
      end type element_parameters_type

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! dummy variables !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


      type(real_one_d_array_type), allocatable :: &
      o(:), ohat(:)
      double precision :: nn_energy
      integer :: i, index, j, m, n, p, k, layer, nn, q, l, no_of_rows, &
      no_of_cols, image_no, no_of_atoms, atom, element! check to see if some of those variables are not used
      double precision :: &
      image_der_parameters_square_error(len_of_parameters)
      type(image_forces_type) :: &
      unraveled_real_forces(no_sub_images(proc))
      type(element_parameters_type) :: unraveled_parameters
      type(element_parameters_type) :: unraveled_der_parameters
      integer :: self_index
      integer :: first_image_no, no_proc_images
      type(real_one_d_array_type), allocatable :: unraveled_o_input(:)

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! calculations !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
      first_image_no = 1
      no_of_atoms = no_of_atoms_of_images(first_image_no)
      do i = 1, proc - 1
        first_image_no = first_image_no + no_sub_images(i)
      end do
      no_proc_images = no_sub_images(proc)
      square_error_energy = 0.0d0
      square_error_forces = 0.0d0
      do i = 1, len_of_parameters
        der_parameters_square_error(i) = 0.0d0
      end do
      call unravel_parameters() ! subroutine
      call unravel_o_input(first_image_no, no_of_atoms) ! subroutine
      if (train_forces .eqv. .true.) then
        call unravel_real_forces(first_image_no, no_proc_images) ! subroutine
      end if

! summation over images

      do image_no = 1, no_proc_images
        do element = 1, len_of_parameters
          image_der_parameters_square_error(element) = 0.0d0
        end do
        no_of_atoms = no_of_atoms_of_images(&
        first_image_no - 1 + image_no)
        nn_energy = calculate_nn_energy(image_no, no_of_atoms,&
        unraveled_parameters, unraveled_o_input) ! function
        square_error_energy = square_error_energy + &
        (nn_energy - &
        real_energies(first_image_no - 1 + image_no)) ** 2.0d0 / &
        (no_of_atoms ** 2.0d0)
        call calculate_der_parameters(first_image_no, image_no, &
        no_of_atoms, unraveled_parameters, nn_energy)
        image_der_parameters_square_error = &
        ravel_parameters(unraveled_der_parameters)
        do i = 1, len_of_parameters
          der_parameters_square_error(i) = &
          der_parameters_square_error(i) &
          + image_der_parameters_square_error(i)
        end do
        
        !deallocations for each image

        do layer = 1, size(unraveled_parameters%weights)
          deallocate(unraveled_parameters%weights(&
          layer)%twodarray)
        end do

        deallocate(unraveled_parameters%weights)


        do layer = 1, size(o)
          deallocate(o(layer)%onedarray)
        end do

        deallocate(o)


        do layer = 1, size(ohat)
          deallocate(ohat(layer)%onedarray)
        end do

        deallocate(ohat)
      end do


!deallocations for all images

 
      do layer = 1, size(unraveled_parameters%weights)
        deallocate(&
        unraveled_parameters%weights(layer)%twodarray)
      end do
      deallocate(unraveled_parameters%weights)
      

      
      if (train_forces .eqv. .true.) then
        do image_no = 1, no_proc_images
          deallocate(unraveled_real_forces(image_no)%atomic_forces)
        end do
      end if

      CONTAINS

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      subroutine unravel_parameters() ! -- checked

      ! type(element_parameters_type) :: unraveled_parameters

        l = 0
        allocate(unraveled_parameters%weights(no_layers_of_nn - 1))
        do j = 1, no_layers_of_nn - 1
          no_of_rows = no_nodes_of_layers(j) + 1
          no_of_cols = no_nodes_of_layers(j + 1)
          allocate(unraveled_parameters%weights(j)%twodarray(&
          no_of_rows, no_of_cols))
          do m = 1, no_of_rows
            do n = 1, no_of_cols
                unraveled_parameters%weights(j)%twodarray(&
                m, n) = parameters(l + (m - 1) * no_of_cols + n)
            end do
          end do
          l = l + no_of_rows * no_of_cols
        end do


          unraveled_parameters%scaling_intercept = &
          parameters(l - 1)
          unraveled_parameters%scaling_slope = &
          parameters(l)
        
      end subroutine unravel_parameters



!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

 
      subroutine unravel_real_forces(first_image_no, no_proc_images)

        integer :: first_image_no, no_proc_images

        k = 0
        if (first_image_no .GT. 1) then
          do image_no = 1, first_image_no - 1
            no_of_atoms = no_of_atoms_of_images(image_no)
            k = k + no_of_atoms
          end do
        end if
        do image_no = 1, no_proc_images
          no_of_atoms = &
          no_of_atoms_of_images(first_image_no - 1 + image_no)
          allocate(unraveled_real_forces(image_no)%atomic_forces(&
          no_of_atoms, 3))
          do atom = 1, no_of_atoms
            do i = 1, 3
              unraveled_real_forces(image_no)%atomic_forces(&
              atom, i) = real_forces(k + atom, i)
            end do
          end do
          k = k + no_of_atoms
        end do
      end subroutine unravel_real_forces


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
      function calculate_nn_energy(image_no, no_of_atoms, & 
      unraveled_parameters, unraveled_o_input) ! -- Checked
 
        integer :: image_no, no_of_atoms
        type(element_parameters_type) :: unraveled_parameters
        type(real_one_d_array_type), allocatable :: unraveled_o_input(:)
        double precision :: calculate_nn_energy
        type(real_one_d_array_type), allocatable :: net(:)
        integer, allocatable :: hiddensizes(:)

     
        allocate(hiddensizes(no_layers_of_nn - 2)) !hiddensizes=array(3, 2)
        do m = 1, no_layers_of_nn - 2
          hiddensizes(m) = no_nodes_of_layers(m + 1)
        end do

        allocate(o(no_layers_of_nn)) !o=array(inputlayer, layer2, ... outputlayer)
        allocate(ohat(no_layers_of_nn)) ! same thing but with bias appended

        layer = 1
        allocate(o(layer)%onedarray(&
        no_of_atoms * 3))
        allocate(ohat(layer)%onedarray(&
        (no_of_atoms * 3) + 1)) ! Add the bias node
        allocate(net(no_layers_of_nn)) !net=array(layer1, layer2, ..., outputlayer)
        do m = 1, size(unraveled_parameters%weights(layer)%&
          twodarray, dim=1) - 1
          o(layer)%onedarray(m) = &
          unraveled_o_input(image_no)%onedarray(m)
        end do
        do layer = 1, size(hiddensizes) + 1
          do m = 1, size(&
          unraveled_parameters%weights(layer)%twodarray, dim=1) - 1
            ohat(layer)%onedarray(m) = &
            o(layer)%onedarray(m)
          end do
          ohat(layer)%onedarray(&
          size(&
          unraveled_parameters%weights(layer)%twodarray, dim=1)) = 1.0d0 !Append the bias node
          allocate(net(layer)%onedarray(&
          size(&
          unraveled_parameters%weights(layer)%twodarray, dim=2)))
          allocate(o(layer + 1)%onedarray(&
          size(&
          unraveled_parameters%weights(layer)%twodarray, dim=2)))
          allocate(ohat(layer + 1)%onedarray(&
          size(&
          unraveled_parameters%weights(layer)%twodarray, dim=2) + 1))
          do m = 1, size(&
          unraveled_parameters%weights(layer)%twodarray, dim=2)
            net(layer)%onedarray(m) = 0.0d0 !Pre-allocate
            do n = 1, size(&
            unraveled_parameters%weights(layer)%twodarray, dim=1)
              net(layer)%onedarray(m) =  &
              net(layer)%onedarray(m) + &
              ohat(layer)%onedarray(n) * &
              unraveled_parameters%weights(layer)%twodarray(n, m)
            end do
            if (activation == 'tanh') then
              o(layer + 1)%onedarray(m) = &
              tanh(net(layer)%onedarray(m))
            else if (activation == 'sigmoid') then
              o(layer + 1)%onedarray(m) = &
              1. / (1. +  exp(- net(layer)%onedarray(m)))
            else if (activation == 'linear') then
              o(layer + 1)%onedarray(m) = &
              net(layer)%onedarray(m)
            else
              print *,&
              "Fortran Error: Unknown activation function!"
            end if
            ohat(layer + 1)%onedarray(m) = &
            o(layer + 1)%onedarray(m)
          end do
          ohat(layer + 1)%onedarray(&
          size(unraveled_parameters%weights(&
          layer)%twodarray, dim=2) + 1) =  1.0d0 ! Append the bias node
        end do
        calculate_nn_energy = &
        unraveled_parameters%scaling_slope * o(&
        layer)%onedarray(1) + &
        unraveled_parameters%scaling_intercept
        
        deallocate(hiddensizes)
        deallocate(net)
       

      end function calculate_nn_energy
 
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine calculate_der_parameters(first_image_no, image_no, &
      no_of_atoms, unraveled_parameters, nn_energy) ! -- Checked
 
        integer :: first_image_no, image_no, no_of_atoms
        double precision :: nn_energy, temp1, temp2
        type(element_parameters_type) :: unraveled_parameters
        type(real_one_d_array_type), allocatable :: delta(:)
        type(real_one_d_array_type), allocatable :: D(:)
        type (embedded_real_one_one_d_array_type), allocatable :: &
        der_coordinates_o(:)
        type(element_parameters_type) :: unraveled_der_parameters
        type (embedded_real_one_two_d_array_type), allocatable :: &
        der_coordinates_weights_atomic_output(:)
        type(real_one_d_array_type), allocatable :: &
        der_coordinates_ohat(:)
        double precision, allocatable :: temp(:)
        type(real_one_d_array_type), allocatable :: der_coordinates_D(:)
        type (real_one_d_array_type), allocatable :: &
        der_coordinates_delta(:)
        double precision :: nn_atomic_forces(no_of_atoms, 3)
        double precision, allocatable :: temp3(:), temp4(:), temp5(:), &
        temp6(:)

        unraveled_der_parameters%scaling_intercept = 0.d0
        unraveled_der_parameters%scaling_slope = 0.d0

        l = 0
        allocate(unraveled_der_parameters%weights(&
        no_layers_of_nn - 1))

        do j = 1, no_layers_of_nn - 1
          no_of_rows = no_nodes_of_layers(j) + 1
          no_of_cols = no_nodes_of_layers(j + 1)
          allocate(unraveled_der_parameters%weights(j)&
          %twodarray(no_of_rows, no_of_cols))
          do m = 1, no_of_rows
            do n = 1, no_of_cols
              unraveled_der_parameters%weights(&
              j)%twodarray(m, n) = 0.0d0
            end do
          end do
          l = l + no_of_rows * no_of_cols
        end do
   

        nn = size(o) - 2
        allocate(D(nn + 1))
        do layer = 1, nn + 1
          allocate(D(layer)%onedarray(&
          size(o(layer + 1)%onedarray)))
          do m = 1, size(o(layer + 1)%onedarray)
            if (activation == "tanh") then
              D(layer)%onedarray(m) = &
              (1.0d0 - o(layer + 1)%onedarray(m)&
               * o(layer + 1)%onedarray(m))
            elseif (activation == "sigmoid") then
              D(layer)%onedarray(m) = &
              o(layer + 1)%onedarray(m) * &
              (1.0d0 - o(layer + 1)%onedarray(m))
            elseif (activation == "linear") then
              D(layer)%onedarray(m) = 1.0d0
            end if
          end do    
        end do
        allocate(delta(nn + 1))
        allocate(delta(nn + 1)%onedarray(1))
        delta(nn + 1)%onedarray(1) = &
        D(nn + 1)%onedarray(1)
        do layer = nn, 1, -1
          allocate(delta(layer)%onedarray(&
          size(D(layer)%onedarray)))
          do m = 1, size(D(layer)%onedarray)
            delta(layer)%onedarray(m) = 0.0d0
            do q = 1, &
            size(delta(layer + 1)%onedarray)
              temp1 = D(layer)%onedarray(m) * &
              unraveled_parameters%weights(layer + 1)&
              %twodarray(m, q)
              temp2 = temp1  * &
              delta(layer + 1)%onedarray(q)
              delta(layer)%onedarray(m) = &
              delta(layer)%onedarray(m) + temp2
            end do
          end do
        end do
    
        unraveled_der_parameters%scaling_intercept = &
        unraveled_der_parameters%scaling_intercept + &
        energy_coefficient *  2.0d0 * (nn_energy - real_energies(&
        first_image_no - 1 + image_no)) / (no_of_atoms ** 2.0d0)
        unraveled_der_parameters%scaling_slope = &
        unraveled_der_parameters%scaling_slope + &
        energy_coefficient *  2.0d0 * &
        (nn_energy - real_energies(first_image_no - 1 + image_no)) * &
        o(nn + 2)%onedarray(1) &
         / (no_of_atoms ** 2.0d0)
        do layer = 1, nn + 1
          do m = 1, size(ohat(layer)%onedarray)
            do n = 1, size(delta(layer)%onedarray)
                unraveled_der_parameters%weights(&
                layer)%twodarray(m, n) = &
                unraveled_der_parameters%weights(layer)%twodarray(m, n) &
                + energy_coefficient *  2. * &
                (nn_energy - real_energies(&
                first_image_no - 1 + image_no)) * &
                unraveled_parameters%scaling_slope &
                * ohat(layer)%onedarray(m) &
                * delta(layer)%onedarray(n) &
                / (no_of_atoms ** 2.0d0)
            end do
          end do
        end do
      

        if (train_forces .eqv. .true.) then
          do self_index = 1, no_of_atoms
            do i = 1, 3
              nn_atomic_forces(self_index, i) = 0.0d0
            end do
          end do
          nn = size(o) - 2
          do self_index = 1, no_of_atoms
            allocate(der_coordinates_weights_atomic_output(3))
            allocate(der_coordinates_o(3)%onedarray(nn + 2))
            allocate(der_coordinates_o(3)%onedarray(1)%onedarray(3 * no_of_atoms))
            ! Pre-allocate der_coordinates_o
            do i = 1, 3
              do j = 1, 3 * no_of_atoms
                der_coordinates_o(i)%onedarray(1)%onedarray(j) = 0.0d0
              end do
            end do
            do i = 1, 3
              der_coordinates_o(i)%onedarray(1)%onedarray((3 * self_index - 1) + i) = 1.0d0
              do layer = 1, nn + 1
                allocate(temp(size(&
                unraveled_parameters%weights(layer)%twodarray, dim = 2)))
                do p = 1, &
                size(unraveled_parameters&
                %weights(layer)%twodarray, dim = 2)
                  temp(p) = 0.0d0
                  do q = 1, &
                  size(unraveled_parameters&
                  %weights(layer)%twodarray, &
                  dim = 1) - 1
                    temp(p) = temp(p) + &
                    der_coordinates_o(i)%onedarray(&
                    layer)%onedarray(q) * &
                    unraveled_parameters&
                    %weights(layer)%twodarray(q, p)
                  end do
                end do             
                q = size(o(&
                layer + 1)%onedarray)
                allocate(der_coordinates_o(i)%&
                onedarray(layer + 1)%onedarray(q))
                do p = 1, &
                size(o(layer + 1)%onedarray)
                  if (activation == 'tanh') then
                    der_coordinates_o(i)%onedarray(&
                    layer + 1)%onedarray(p) = temp(p) * &
                    (1.0 - o(layer + 1)&
                    %onedarray(p) * o(&
                    layer + 1)%onedarray(p))
                  else if (activation == 'sigmoid') then
                    der_coordinates_o(i)%onedarray(&
                    layer + 1)%onedarray(p) = temp(p) * &
                    (1.0 - o(layer + 1)%&
                    onedarray(p)) * o(&
                    layer + 1)%onedarray(p)
                  else if (activation == 'linear') then
                    der_coordinates_o(i)%onedarray(layer&
                     + 1)%onedarray(p) = temp(p)
                  end if
                end do
                deallocate(temp)
              end do
              allocate(der_coordinates_D(nn + 1))
              do layer = 1, nn + 1
                allocate(der_coordinates_D(layer)%onedarray(&
                size(o(&
                layer + 1)%onedarray)))
                do m = 1, size(o(&
                layer + 1)%onedarray)
                  if (activation == "tanh") then
                    der_coordinates_D(layer)%onedarray(m) =&
                    - 2.0d0 * o(&
                    layer + 1)%onedarray(m) * &
                    der_coordinates_o(i)%onedarray(&
                    layer + 1)%onedarray(m)
                  elseif (activation == "sigmoid") then
                    der_coordinates_D(layer)%onedarray(m) =&
                    der_coordinates_o(i)%onedarray(&
                    layer + 1)%onedarray(m) * &
                    ( 1.0d0 - 2.0d0 * o(&
                    layer + 1)%onedarray(m))
                  elseif (activation == "linear") then
                    der_coordinates_D(layer)%onedarray(m) =&
                    0.0d0
                  end if
                end do    
              end do
              allocate(der_coordinates_delta(nn + 1))
              allocate(der_coordinates_delta(nn + 1)%onedarray(1))
              der_coordinates_delta(nn + 1)%onedarray(1) = &
              der_coordinates_D(nn + 1)%onedarray(1)
              do layer = nn, 1, -1
                allocate(temp3(size(unraveled_parameters&
                %weights(&
                layer + 1)%twodarray, dim = 1) - 1))
                allocate(temp4(size(unraveled_parameters&
                %weights(&
                layer + 1)%twodarray, dim = 1) - 1))
                do p = 1, size(unraveled_parameters&
                %weights(&
                layer + 1)%twodarray, dim = 1) - 1
                  temp3(p) = 0.0d0
                  temp4(p) = 0.0d0
                  do q = 1, size(delta(&     
                  layer + 1)%onedarray)
                    temp3(p) = temp3(p) + &
                    unraveled_parameters&
                    %weights(layer + 1)%twodarray(&
                    p, q) * delta(&
                    layer + 1)%onedarray(q)
                    temp4(p) = temp4(p) + &
                    unraveled_parameters&
                    %weights(layer + 1)%twodarray(&
                    p, q) * der_coordinates_delta(&
                    layer + 1)%onedarray(q)
                  end do
                end do
                allocate(temp5(&
                size(der_coordinates_D(layer)%onedarray)))
                allocate(temp6(size(&
                der_coordinates_D(layer)%onedarray)))
                allocate(der_coordinates_delta(&
                layer)%onedarray(size(der_coordinates_D(&
                layer)%onedarray)))
                do p = 1, size(&
                der_coordinates_D(layer)%onedarray)
                  temp5(p) = der_coordinates_D(layer)%&
                  onedarray(p) * temp3(p)
                  temp6(p) = D(&
                  layer)%onedarray(p) * temp4(p)
                  der_coordinates_delta(layer)%onedarray(p)= &
                  temp5(p) + temp6(p)
                end do
                deallocate(temp3)
                deallocate(temp4)
                deallocate(temp5)
                deallocate(temp6)
              end do
              allocate(der_coordinates_ohat(nn + 1))
              allocate(der_coordinates_weights_atomic_output(&
              i)%onedarray(nn + 1))
              do layer = 1, nn + 1
                allocate(der_coordinates_ohat(layer)%onedarray(&
                size(der_coordinates_o(i)%onedarray(layer)%&
                onedarray) + 1))
                do p = 1, size(der_coordinates_o(i)%&
                onedarray(layer)%onedarray)
                  der_coordinates_ohat(layer)%onedarray(p) = &
                  der_coordinates_o(i)%onedarray(&
                  layer)%onedarray(p)
                end do
                der_coordinates_ohat(layer)%onedarray(&
                size(der_coordinates_o(i)%onedarray(&
                layer)%onedarray) + 1) = 0.0d0
                allocate(der_coordinates_weights_atomic_output(&
                i)%onedarray(layer)%twodarray&
                (size(der_coordinates_ohat(layer)%onedarray), &
                size(&
                delta(layer)%onedarray)))
                do p = 1, size(&
                der_coordinates_ohat(layer)%onedarray)
                  do q = 1, size(delta(&
                  layer)%onedarray)
                    der_coordinates_weights_atomic_output&
                    (i)%onedarray(layer)%twodarray(p, q)&
                    = 0.0d0
                  end do
                end do
                do p = 1, size(&
                der_coordinates_ohat(layer)%onedarray)
                  do q = 1, size(&
                  delta(layer)%onedarray)
                    der_coordinates_weights_atomic_output(i)&
                    %onedarray(layer)%twodarray(p, q) = &
                    der_coordinates_weights_atomic_output(i)&
                    %onedarray(layer)%twodarray(p, q) + &
                    der_coordinates_ohat(layer)%onedarray(p) * &
                    delta(layer)%&
                    onedarray(q) + &
                    ohat(layer)%onedarray(p)&
                    * der_coordinates_delta(layer)%onedarray(q)
                  end do
                end do 
              end do
              nn_atomic_forces(self_index, i) = &
              nn_atomic_forces(self_index, i) - &
              unraveled_parameters%scaling_slope*&
              der_coordinates_o(i)%onedarray(nn + 2)%&
              onedarray(1)
              do p = 1, size(der_coordinates_ohat)
                deallocate(der_coordinates_ohat(p)%onedarray)
              end do
              deallocate(der_coordinates_ohat)
              do p = 1, size(der_coordinates_delta)
                deallocate(der_coordinates_delta(p)%onedarray)
              end do
              deallocate(der_coordinates_delta)   
              do p = 1, size(der_coordinates_D)
                deallocate(der_coordinates_D(p)%onedarray)
              end do
              deallocate(der_coordinates_D)
            end do
          enddo
          do i = 1, 3
            square_error_forces = square_error_forces + &
            (1.0d0 / 3.0d0)*(nn_atomic_forces(self_index, i) - &
            unraveled_real_forces(image_no)%&
            atomic_forces(self_index, i)) ** 2.0 / no_of_atoms
            nn = size(o) - 2          
            do layer = 1, nn + 1
              do m = 1, size(&
                ohat(layer)%onedarray)
                do n = 1, size(&
                delta(layer)%onedarray)
                  unraveled_der_parameters%&
                  weights(layer)%twodarray(m, n) = &
                  unraveled_der_parameters%&
                  weights(layer)%twodarray(m, n) &
                  + force_coefficient * (2.0d0 / 3.0d0) * &
                  unraveled_parameters&
                  %scaling_slope*&
                  (- nn_atomic_forces(self_index, i)  + &
                  unraveled_real_forces(image_no)%&
                  atomic_forces(self_index, i)) * &
                  der_coordinates_weights_atomic_output(i)&
                  %onedarray(layer)%twodarray(m, n) &
                  / no_of_atoms
                end do
              end do
            end do
            unraveled_der_parameters%scaling_slope = &
            unraveled_der_parameters%scaling_slope + &
            force_coefficient * (2.0d0 / 3.0d0) * &
            der_coordinates_o(i)%onedarray(nn + 2)%&
            onedarray(1) * (- nn_atomic_forces(self_index, i) +&
            unraveled_real_forces(image_no)%&
            atomic_forces(self_index, i)) / no_of_atoms
          enddo    
        end if
          


  ! Deallocate memory

        do i = 1, 3
          do j = 1, size(der_coordinates_o(i)%onedarray)
            deallocate(&
            der_coordinates_o(i)%onedarray(j)%onedarray)
          end do
          deallocate(der_coordinates_o(i)%onedarray)
        end do
        deallocate(der_coordinates_o)

        do i = 1, 3
          do j = 1, size(&
          der_coordinates_weights_atomic_output(i)%&
          onedarray)
            deallocate(&
            der_coordinates_weights_atomic_output(i)%&
            onedarray(j)%twodarray)
          end do
          deallocate(&
          der_coordinates_weights_atomic_output(i)%&
          onedarray)
        end do
        
        deallocate(der_coordinates_weights_atomic_output)


        do i = 1, size(delta)
          deallocate(delta(i)%onedarray)
        end do
        deallocate(delta)

        do i = 1, size(D)
          deallocate(D(i)%onedarray)
        end do
        deallocate(D)


      end subroutine calculate_der_parameters

 

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

      subroutine unravel_o_input(first_image_no, no_of_atoms) ! -- Checked

        type(real_one_d_array_type) :: unraveled_o_input(no_proc_images)
        integer :: proc, first_image_no, no_of_atoms

        ! do image_no = 1, no_proc_images
        !   no_of_atoms = &
        !   no_of_atoms_of_images(first_image_no - 1 + image_no)
        !   allocate(unraveled_o_input(&
        !   image_no)%onedarray(3 * no_of_atoms))
        !   do index = 1, 3 * no_of_atoms
        !     unraveled_o_input(&
        !     image_no)&
        !     %onedarray(index) = input_positions_of_sub_images&
        !     (proc, image_no, index)
        !   end do
        ! end do

        do image_no = 1, no_proc_images ! all images have the same number of atoms
          allocate(unraveled_o_input(image_no)%&
            onedarray(3 * no_of_atoms))
          do index = 1, 3 * no_of_atoms
            unraveled_o_input(image_no)&
            %onedarray(index) = input_positions_of_images(first_image_no - 1 + image_no, index)
          end do
        end do

      end subroutine unravel_o_input

 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  
      function ravel_parameters(unraveled_der_parameters) ! -- Checked
 
        double precision :: ravel_parameters(len_of_parameters)
        type(element_parameters_type) :: unraveled_der_parameters
        
        l = 0
        do j = 1, no_layers_of_nn - 1
          no_of_rows = no_nodes_of_layers(j) + 1
          no_of_cols = no_nodes_of_layers(j + 1)
          do m = 1, no_of_rows
            do n = 1, no_of_cols
              ravel_parameters(l + (m - 1) * no_of_cols + n) = &
              unraveled_der_parameters%weights(j)%&
              twodarray(m, n)
            end do
          end do
          l = l + no_of_rows * no_of_cols
        end do
        
        
        ravel_parameters(l - 1) = &
        unraveled_der_parameters%scaling_intercept
        ravel_parameters(l) = &
        unraveled_der_parameters%scaling_slope
      
      end function ravel_parameters
      
 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      
    end subroutine feedforward_backpropagation
  
      
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!