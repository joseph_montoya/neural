"""
This simple script aims to check the python neural network against an analytic
analog in mathematica for accuracy. It checks the output o, the Energy E and the SquareError.
"""

# Imports


def testCartesianTrain():
    import numpy as np
    from neural.cartesian import CartesianNeural
    from neural.cartesian import FeedForward
    from ase import io
    from collections import OrderedDict
    import multiprocessing as mp
    import os
    from ase import Atoms, Atom
    from ase.calculators.emt import EMT

    training_images = []

    def generate_images(count):

        for step in range(count):
            '''Generates five images with varying configurations'''
            image = Atoms([Atom('Pd', (0., 0., 0.)), Atom('O', (0., 2., 0.)),
                Atom('Pd', (0., 0., 3.)), Atom('Pd', (1., 0., 0.))])

            if step > 0:
                image[step - 1].position =  \
                image[step - 1].position + np.array([0., 1., 1.])

            image.set_calculator(EMT())
            image.get_potential_energy(apply_constraint=False)
            image.get_forces(apply_constraint=False)
            training_images.append(image)

    # Generate the 'training' images

    generate_images(5)

    weights = OrderedDict([(1, np.array([[1., 2.5],
                                         [0., 1.5],
                                         [0., -1.5],
                                         [3., 9.],
                                         [1., -2.5],
                                         [2., 3.],
                                         [2., 2.5],
                                         [3., 0.],
                                         [-3.5, 1.],
                                         [5., 3.],
                                         [-2., 2.5],
                                         [-4., 4.],
                                         [0., 0.]])), 
                                         (2, np.array([[1.],
                                                       [2.],
                                                       [0.]])),
                                                       (3, np.array([[3.5],
                                                                     [0.]]))])
    hiddenlayers = (2, 1)

    scalings = OrderedDict([('intercept', 3.),
                            ('slope', 2.)])

    activation = 'tanh'

    # Check the outputs o, E from the Neural Network for all 5 images
    # together with the gradient of the cost fxn and the cost fxn itself

    o_s = []
    Es = []

    mathematica_grad_cost_fxn = [-0.0748715826202491,
                                -3.2658471768376900,
                                0.0361138579403264,
                                8.1576544345198500,
                                0.0370952503061369,
                                8.3716687534740700,
                                0.0001811370967191,
                                0.0362548514911195,
                                -2.3046464944080500,
                                -43.0907932714364000,
                                -0.7901252717117910,
                                -0.5966279491272150,
                                0.0000807018841964,
                                0.0126859109173684,
                                0.0012414556099832,
                                -0.3961157983775950,
                                -2.2719686704217500,
                                -64.4613342756628000,
                                -0.6827254134738740,
                                -18.0665380621386000,
                                -0.0004949589187430,
                                -0.1262892524494970,
                                -0.0003787142390900,
                                -0.1069216354597530,
                                -0.7576833666095130,
                                -21.3599636388309000,
                                 27.0384258732968000,
                                -21.4077767673396000,
                                -27.4733462087644000,
                                -5.9454023914346700,
                                -11.1713190585716000,
                                -89.3943577958031000,
                                -86.5166825638690000
                                ]

    mathematica_energies = [
        4.867131012402346,
        4.867131012402346,
        4.991184749099429,
        4.976000413790214,
        4.980739289325503]  # obtained from mathematica script
    mathematica_os = [
        0.9335655062011731,
        0.9335655062011731,
        0.9955923745497146,
        0.988000206895107,
        0.9903696446627515]  # obtained from mathematica script
    square_error_energy_mathematica = [
        3199.0527662215736,
        24.61269249131737,
        3280.5799195475247,
        3237.61554804185,
        9.554481244618035]  # obtained from mathematica script
    square_error_forces_mathematica = [
        104497.24820961579,
        819.5410428375304,
        104480.43562362246,
        104527.37890557194,
        647.0618234348974]  # obtained from mathematica script
    square_error_mathematica = [
        7378.942694606205,
        57.39433420481859,
        7459.797344492423,
        7418.710673228432,
        35.436954182013935]  # obtained from mathematica script

    SumSquareErrorEnergy_mathematica = 0.
    for elem in square_error_energy_mathematica:
        SumSquareErrorEnergy_mathematica += elem

    SumSquareErrorForces_mathematica = 0.
    for elem in square_error_forces_mathematica:
        SumSquareErrorForces_mathematica += elem

    SumSquareError_mathematica = 0.
    for elem in square_error_mathematica:
        SumSquareError_mathematica += elem

    rmse_energy_per_atom = np.sqrt(SumSquareErrorEnergy_mathematica / 20)
    rmse_force_per_atom = np.sqrt(SumSquareErrorForces_mathematica / 20)

    print '\nSumSquareError_mathematica is %4.8f' % SumSquareError_mathematica
    print 'SumSquareErrorEnergy_mathematica is %4.8f' % SumSquareErrorEnergy_mathematica
    print 'SumSquareErrorForces_mathematica is %4.8f' % SumSquareErrorForces_mathematica
    print 'rmse_energy_per_atom in eV/atom is %4.8f' % rmse_energy_per_atom
    print 'rmse_force_per_atom in eV/atom is %4.8f\n\n' % rmse_force_per_atom

    for index, image in enumerate(training_images):
        ff = FeedForward(hiddenlayers, weights, scalings, activation)
        o, E = ff.get_output(image)
        o_s.append(o)
        Es.append(E)
        # Check for consistency between the two models
        assert (abs(o_s[index][3] - mathematica_os[index]) < 10. **
                (-5.)), 'The output, o from the NN is wrong!'
        assert (abs(Es[index] - mathematica_energies[index]) < 10. **
                (-5.)), 'The Energy, E from the NN is wrong!'

    # Initialize the Cartesian Calculator

    for fortran in [False]:
        for cores in range(1, mp.cpu_count() + 1):

            calc = CartesianNeural(
                cores=cores,
                no_of_atoms=4,
                hiddenlayers=(
                    2,
                    1),
                weights=weights,
                scalings=scalings,
                fortran=fortran,
                label='fortran=%s;cores=%i' % (fortran, cores))

            # Train the calc on the five homogeneous images above with extremely
            # low accuracy

            calc.train(images=training_images,
                       energy_goal=10. ** 10.,
                       force_goal=10. ** 10.)


            # Check for consistency between the two models

            for index, _ in enumerate(mathematica_grad_cost_fxn):
                assert  (abs(_ - calc._der_cost_fxn[index]) < 1. * 10 ** (-12.)), \
                'The value of the gradient of the cost function is wrong for parameter %4.16f' % (calc._der_cost_fxn[index])
            
            assert (abs(calc._cost_function - SumSquareError_mathematica) <
                    5. * 10. ** (-5.)), 'The cost function value from the NN is wrong!'
            assert (abs(calc._rms_energy_error_per_atom -
                        rmse_energy_per_atom) < 5. * 10 ** (-5.)), \
                          'The rmse energy per atom from NN is wrong!'
            assert (abs(calc._rms_force_error_per_atom - rmse_force_per_atom) <
                    5. * 10. ** (-5.)), 'The rmse force per atom from NN is wrong!'
            

if __name__ == '__main__':
    testCartesianTrain()
