
# Installation

Neural is python-based and is designed to integrate closely with the [Atomic Simulation Environment](https://wiki.fysik.dtu.dk/ase/) (ASE). 

## Install ASE

We always test against the latest version (svn checkout) of ASE, but slightly older versions (>=3.9.0) are likely to work as well. Follow the instructions at the ASE website:

[https://wiki.fysik.dtu.dk/ase/download.html](https://wiki.fysik.dtu.dk/ase/download.html)

ASE itself depends upon python with the standard numeric and scientific packages. Verify that you have working versions of [NumPy](http://numpy.org) and [SciPy](http://scipy.org). We also recommend [matplotlib](http://matplotlib.org) in order to make the plots in the examples in this documentation.

## Check out the code

As a relatively new project, we do not yet have stable releases. However, we run daily unit tests to make sure that our development code works as intended. We recommend checking out the latest version of the code via [the project's bitbucket page](https://bitbucket.org/andrewpeterson/neural/). If you use git, check out the code with:

```bash
cd ~/path/to/my/codes
git clone git@bitbucket.org:andrewpeterson/neural.git
```

where you should replace '~/path/to/my/codes' with wherever you would like the code to be located on your computer. If you don't use git, just download the code as a zip file from the project's [download page](https://bitbucket.org/andrewpeterson/neural/downloads), and extract it into '~/path/to/my/codes'.

## Set the environment

You need to let your python version know about the existence of the *Neural* module. Add the following line to your '.bashrc' (or other appropriate spot), with the appropriate path substituted for '~/path/to/my/codes':

```bash
export PYTHONPATH=~/path/to/my/codes:$PYTHONPATH
```

You can check that this works by starting python and typing the below command, verifying that the location listed from the second command is where you expect.

```python
>>> import neural
>>> print(neural.__file__)
```

## Recommended step: Fortran modules

The code is designed to work in pure python, which makes it is easier to read, develop, and debug. However, it will be annoyingly slow unless you compile the associated fortran modules which speed up some crucial parts of the code. The compilation of the fortran code and integration with the python parts is accomplished with the command 'f2py', which is part of [NumPy](http://numpy.org). A fortran compiler will also be necessary on your system; a reasonable open-source option is GNU Fortran, or gfortran. This compiler will be used by f2py to generate compiled Fortran modules (.so) on Linux as follows:

```bash
f2py -c -m fmodules fmodules.f
```

On a Windows machine, the following line generates compiled Fortran modules (.pyd):

```bash
f2py –c –m fmodules fmodules.f --fcompiler=gnu95 --compiler=mingw32
```

If you update the code and your compiled version of the fmodules is not updated, an exception will be raised, telling you to re-compile. For a detailed description of the process, please see the README file.


## Recommended step: Run the tests

We include tests in the code to ensure that it still runs as intended as we continue our development; we run these tests on the latest build every night to try to keep bugs out. It is a good idea to run these tests after you install the code to see if your installation is working. The tests are in the folder `tests`; they are designed to run with [nose](https://nose.readthedocs.org/). If you have nose installed, run the commands below.

```bash
mkdir /tmp/neuraltests
cd /tmp/neuraltests
nosetests ~/path/to/my/codes/neural
```




