""" This package contains different neural calculators.

Prepared by Andrew Peterson* and Alireza Khorshidi (Sept. 2014)
School of Engineering, Brown University, Providence, RI, USA, 02912
*: Andrew_Peterson@brown.edu

See the accompanying license file for details.

"""

from neural.cartesian import CartesianNeural as Neural
