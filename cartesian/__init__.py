#!/usr/bin/env python
"""
This module contains the CartesianNeural calculator, an ASE-like calculator
for implementing a basic neural network based on the Cartesian coordinates
of the atoms in the system. This will only work if the number and
identities of atoms in the systems under study do not change -- only the
atomic positions can change.

"""

######################################################################

import numpy as np
from scipy.optimize import fmin_bfgs as optimizer
import time
from ase.calculators.neighborlist import NeighborList
from ase.calculators.calculator import Calculator # import ase Calculator class
# for inheritance purpose
from ase.data import atomic_numbers
from ase.parallel import paropen
from collections import OrderedDict
from ase import io
import os
import json
import warnings
import gc
import multiprocessing as mp
from ..utilities import hash_image, Logger

try:
   from .. import cartesianfmodules
   # version 2 of cartesianfmodules
   cartesianfmodules_version = 2
   warning = cartesianfmodules.check_version(version=cartesianfmodules_version)
   if warning == 1:
       raise RuntimeError('Fortran part is not updated. Please recompile'
                          ' with f2py as described in the README. '
                          'Correct version is %i.' % cartesianfmodules_version)
except ImportError:
   cartesianfmodules = None
   warnings.warn('Not using the Fortran 90 part of the code. '
                 'Some parts may be very slow! Compile as described '
                 'in the README for improved performance.')

##############################################################################


class CartesianNeural(Calculator):

    """Sets up a neural network based on cartesian coordinates of the
    atoms (as opposed to the Behler-Parinelllo method). This method does
    not allow one to add or subtract atoms, but is useful for representing
    a local potential energy surface. All atoms must have identical
    numbering and unit cell.
    Here, extrapolate is defined by any cartesian coordinate (x, y, z) of
    an atom being in a region that is not covered by the scale argument. By
    default this is set to True to allow this to occur, but can be set to
    'truncate' to return the bounds instead of outliers or False to raise
    an exception when a user attempts to extrapolate.

    Inputs:
        json: string
            output JSON file, or input file if JSON already exists
        hiddenlayers: Tuple that conveys the number of hiddenlayers in
                    the NN architecture and eventually the number of nodes
                    in each layer.
                    For example, hiddenlayers = (3,5) means that
                    there exists two hidden layers, the first one with
                    three nodes and the second one having five nodes.
        activation: string to assign the type of activation funtion. "linear"
                    refers to linear function, "tanh" refers to tanh
                    function, and "sigmoid" refers to sigmoid function.
        label: string
            name used for all files.
        weights:    Dictionary of arrays of weights connecting one layer to the
                    next. Layers are indexed from 0
                    while the weight dictionary has key values starting from 1.
                    There is one less weight array than there are layers.
                    An example is shown below:
                    weights = {1: np.array([[1,1],[2,4],[4,5],[6,1]]),
                    2: np.array([[4],[6],[1]])}.
                    The arrays are set up
                    to connect node i in the previous layer with node j in the
                    current layer with indices w[i,j]. There are n+1 rows
                    (i values), with the last row being the bias, and m columns
                    (j values). If weights are not given, the arrays will be
                    randomly generated from values between -0.5 and 0.5.
        scalings:   Dictionary of parameters for slope and intercept.
                    They are used in order to remove the final value
                    from the range that the activation function would
                    otherwise be locked in. For example,
                    scalings = {"slope":2, "intercept":1}
        extrapolate: boolean
            If True, allows for extrapolation, if False, does not allow.
    Output:
        energy: float
        forces: matrix of floats

    **NOTE: The dimensions of the weight matrix should be consistent with the
            hiddenlayers.**

    """

    implemented_properties = ['energy', 'forces']

    ##########################################################################

    def __init__(self, cores, fortran=True, no_of_atoms=None, json=None, hiddenlayers=(5, 5),
                 activation='tanh', label=None, weights=None, scalings=None,
                 extrapolate=True):

        self._no_of_atoms = no_of_atoms
        self._hiddenlayers = hiddenlayers
        self._activation = activation
        self._weights = weights
        self._cores = cores
        self._fortran = fortran
        self._scalings = scalings
        self._label = label
        self._extrapolate = extrapolate

        # Reading parameters fron json if any:
        if json is not None:
            json_file = paropen(json, 'rb') # read binary file
            self._no_of_atoms, self._activation, self._hiddenlayers, \
                self._coordinates_range, self._weights, self._scalings = \
                read_parameters_json(json_file)

        # Checking that the activation function is given correctly:
        string = 'Unknown activation function; activation must be \
                    either "linear", "tanh", or "sigmoid".'
        if self._activation != 'linear' and self._activation != 'tanh' \
                and self._activation != 'sigmoid':
            raise NotImplementedError(string)
        del string

        # Checking the compatibility of the forms of coordinates, hiddenlayers
        # and weights:
        string1 = 'number of atoms and weights are not compatible.'
        string2 = 'hiddenlayers and weights are not compatible.'

        if self._weights is not None:
            if no_of_atoms is not None:
                if np.shape(self._weights[1])[0] != 3 * self._no_of_atoms + 1:
                    raise RuntimeError(string1)
            if isinstance(self._hiddenlayers, int):
                if np.shape(self._weights[1])[1] != self._hiddenlayers:
                    raise RuntimeError(string2)
            else:
                if np.shape(self._weights[1])[1] != self._hiddenlayers[0]:
                    raise RuntimeError(string2)
                for i in range(2, len(self._hiddenlayers) + 1):
                    if np.shape(self._weights[i])[0] != \
                        self._hiddenlayers[i - 2] + 1 or \
                        np.shape(self._weights[i])[1] != \
                            self._hiddenlayers[i - 1]:
                        raise RuntimeError(string2)
        del string1
        del string2

        Calculator.__init__(self, self._label)

    #########################################################################

    def initialize(self, atoms):
        self.par = {}
        self.rc = 0.0
        self.numbers = atoms.get_atomic_numbers()
        self.forces = np.empty((len(atoms), 3))
        self.nl = NeighborList([0.5 * self.rc + 0.25] * len(atoms),
                               self_interaction=False)

    #########################################################################

    def calculate(self, atoms, properties, system_changes):
        """ Calculation of the energy of the system and forces of all atoms."""

        # set the atoms attribute by calling calculate method for Calculator base
        # class

        Calculator.calculate(self, atoms, properties, system_changes)

        ff = FeedForward(self._hiddenlayers, self._weights,
                         self._scalings, self._activation)

        if self._extrapolate is False:
            if compare_train_test_coordinates(atoms,
                                              self._coordinates_range) == 1:
                raise ExtrapolateError('Trying to extrapolate, which'
                                       ' is not allowed. Change to '
                                       'extrapolate=True if this is'
                                       ' desired.')

        if self._weights is None:
            raise RuntimeError("Calculator not trained; can't return"
                               'properties.')

        if 'numbers' in system_changes:
            self.initialize(atoms)

        self.nl.update(atoms)

        if properties == ['energy']:
            oo, Ei = ff.get_output(atoms)
            self.energy = Ei
            self.results['energy'] = float(self.energy)

        if properties == ['forces']:
            self.forces[:] = 0.0
            gf = GetForces(atoms, self._hiddenlayers, self._weights,
                           self._scalings, self._activation)
            for atom in atoms:
                index = atom.index
                self.forces[index][0] = gf.calculate_force(index, 0)
                self.forces[index][1] = gf.calculate_force(index, 1)
                self.forces[index][2] = gf.calculate_force(index, 2)
            self.results['forces'] = self.forces

    #########################################################################

    def train(
            self,
            images,
            energy_goal= 0.003,
            force_goal= None
            ):
        """Fits a parameter set to the data, using "fmin_bfgs" optimizer,
        that takes as input a cost function to reduce and an initial guess of
        parameters and returns an optimized parameter set. The convergence
        criterion is both root-mean square of energy fit (eV) per atom and
        root-mean square of force fit per atom.
        Inputs:
            images: list of ASE atoms objects
                    List of objects with positions, symbols, and energies in
                    ASE form. this is the training set of data. This can
                    also be the path to an ASE trajectory (.traj) or
                    database (.db) file.  Energies can be obtained from any
                    reference, e.g. DFT calculations.
            goal: threshold rmse/image/atom at which simulation is converged
            train_forces: if "True", forces will be trained as well

        """

        if force_goal is None:
            train_forces = False
        else:
            train_forces = True
        
        if self._no_of_atoms is None:
            self._no_of_atoms = len(images[0])

        log = Logger(make_filename(self._label, 'train-log.txt'))

        log('Neural: Cartesian training started. ' + time.ctime())
        log('Parallel processing over %i cores.\n' % self._cores)

        if isinstance(images, str):
            extension = os.path.splitext(images)[1]
            if extension == '.traj':
                images = io.Trajectory(images, 'r')
            elif extension == '.db':
                images = io.read(images)

        for image in images:
            if len(image) != self._no_of_atoms:
                raise RuntimeError('Either "no_of_atoms" keyword is incorrect'
                                   'or number of atoms in different images'
                                   'is not the same.  In the latter case try'
                                   'BPNeural')

        log('Training on %i images.' % len(images))

        # Images is converted to dictionary form; key is hash of image.
        log.tic()
        log('Hashing images...')
        dict_images = {}
        for image in images:
            key = hash_image(image)
            if key in dict_images.keys():
                log('WARNING: Duplicate image (based on identical hash).'
                    ' Was this expected? Hash: %s' % key)
            dict_images[key] = image
        images = dict_images.copy()
        del dict_images
        hash_keys = sorted(images.keys())
        log(' ...hashing completed.', toc=True)

        # Chemical elements existing in the image set are printed out.
        elements = []
        for hash_key in hash_keys:
            for atom in images[hash_key]:
                if atom.symbol not in elements:
                    elements.append(atom.symbol)
        self._elements = sorted(elements)

        msg = '%i unique elements included:' % len(self._elements)
        msg += '  ' + ', '.join(self._elements)
        log(msg)

        # If weights are not given, generates random weights
        if not self._weights:
            log('Initializing with random weights.')
            self._weights = make_weight_matrices(self._no_of_atoms,
                                                 self._hiddenlayers,
                                                 self._activation)
        # If scalings are not given, generates random scalings
        if not self._scalings:
            log('Initializing with random scalings.')
            self._scalings = make_scalings_matrices(images, self._activation)

        # Range of coordinates is calculated
        min_x = min(image.positions[atom.index][0]
                    for hash_key, image in images.items()
                    for atom in image)
        min_y = min(image.positions[atom.index][1]
                    for hash_key, image in images.items()
                    for atom in image)
        min_z = min(image.positions[atom.index][2]
                    for hash_key, image in images.items()
                    for atom in image)
        minimum_coords = [min_x, min_y, min_z]

        max_x = max(image.positions[atom.index][0]
                    for hash_key, image in images.items()
                    for atom in image)
        max_y = max(image.positions[atom.index][1]
                    for hash_key, image in images.items()
                    for atom in image)
        max_z = max(image.positions[atom.index][2]
                    for hash_key, image in images.items()
                    for atom in image)
        maximum_coords = [max_x, max_y, max_z]

        self._coordinates_range = [minimum_coords, maximum_coords]

        ravel = ParamRavel(self._weights, self._scalings)

        costfxn = CostFxnandDer(
            self._no_of_atoms,
            self._hiddenlayers,
            ravel,
            hash_keys,
            images,
            self._activation,
            self._label,
            log,
            energy_goal,
            force_goal,
            self._coordinates_range,
            train_forces,
            self._cores,
            self._fortran)

        vector = ravel.to_vector(self._weights, self._scalings)
        # The initial guess


        # saving initial parameters
        filename = make_filename(self._label, 'initial-parameters.json')
        save_parameters_json(filename, self._no_of_atoms,
                             self._activation, self._hiddenlayers,
                             self._coordinates_range, ravel, vector)

        log('Starting optimization of cost function...')
        log.tic()
        converged = False
        try:
            optimizer(f=costfxn.f, x0=vector, fprime=costfxn.fprime,
                      gtol=10. ** -500.)

        except ConvergedException:
            converged = True

        vector = ravel.to_vector(costfxn._weights, costfxn._scalings)
        filename = make_filename(self._label, 'trained-parameters.json')
        save_parameters_json(filename, self._no_of_atoms,
                             self._activation, self._hiddenlayers,
                             self._coordinates_range, ravel, vector)

        if converged:
            self._weights = costfxn._weights
            self._scalings = costfxn._scalings
            log(' ...optimization completed successfully. Optimal '
                'parameters saved.', toc=True)
        else:
            log(' ...could not find parameters for the desired goal\n'
                'error, however, least-error parameters are saved.\n'
                'Try it again or assign a larger value for "goal".',
                toc=True)

        self._cost_function = costfxn._cost_function
        self._rms_energy_error_per_atom = costfxn._rms_energy_error_per_atom
        self._rms_force_error_per_atom = costfxn._rms_force_error_per_atom
        self._square_error_forces = costfxn._square_error_forces
        self._der_cost_fxn = costfxn._der_cost_fxn

##############################################################################
##############################################################################
##############################################################################


class FeedForward:

    """
    Class that implements a basic feed-forward neural network.
    Task:  Constructs the feed-forward neural
            network and calculates the output (energy).
    Inputs:
        hiddenlayers: Tuple that conveys the number of hiddenlayers in
                    the NN architecture and eventually the number of nodes
                    in each layer.
                    For example, hiddenlayers = (3,5) means that
                    there exists two hidden layers, the first one with
                    three nodes and the second one having five nodes.
        weights:    Dictionary of arrays of weights connecting one layer to the
                    next. Layers are indexed from 0
                    while the weight dictionary has key values starting from 1.
                    There is one less weight array than there are layers.
                    An example is shown below:
                    weights = {1: np.array([[1,1],[2,4],[4,5],[6,1]]),
                    2: np.array([[4],[6],[1]])}.
                    The arrays are set up
                    to connect node i in the previous layer with node j in the
                    current layer with indices w[i,j]. There are n+1 rows
                    (i values), with the last row being the bias, and m columns
                    (j values). If weights are not given, the arrays will be
                    randomly generated from values between -0.5 and 0.5.
        scalings:   Dictionary of parameters for slope and intercept.
                    They are used in order to remove the final value
                    from the range that the activation function would
                    otherwise be locked in. For example,
                    scalings = {"slope":2, "intercept":1}
        activation: string to assign the type of activation funtion. "linear"
                    refers to linear function, "tanh" refers to tanh function,
                    and "sigmoid" refers to sigmoid function.


    **NOTE: The dimensions of the weight matrix should be consistent with
            hiddenlayers.**
    """

    def __init__(self, hiddenlayers, weights, scalings, activation):

        self._weights = weights
        self._scalings = scalings
        self._activation = activation
        if isinstance(hiddenlayers, int):
            self._hiddensizes = [hiddenlayers]
        else:
            self._hiddensizes = [int(part) for part in list(hiddenlayers)]

    #########################################################################

    def get_output(self, image):
        """Given cartesian coordinates for each atom, outputs of
        the neural network are calculated."""

        hiddensizes = self._hiddensizes
        weight = self._weights
        o = OrderedDict()  # node values
        layer = 1  # input layer
        if cartesianfmodules:
            o[1] = cartesianfmodules.process_o(
                o_input=np.matrix((image.positions).ravel()),
                weight_no_row=np.shape(weight[1])[0],
                weight_no_col=np.shape(weight[1])[1],
                weight=weight[1],
                activation_function=self._activation)
        else:
            net = OrderedDict()  # excitation
            ohat = OrderedDict()
            o[0] = np.matrix((image.positions).ravel())
            ohat[0] = np.hstack((o[0], [[1.0]]))
            net[1] = ohat[0] * weight[1]
            if self._activation == 'linear':
                o[1] = net[1]  # linear activation
            elif self._activation == 'tanh':
                o[1] = np.tanh(net[1])  # tanh activation
            elif self._activation == 'sigmoid':  # sigmoid activation
                o[1] = 1. / (1. + np.exp(-net[1]))
            ohat[1] = np.hstack((o[1], np.matrix([[1.0]])))
        for hiddensize in hiddensizes[1:]:
            layer += 1
            if cartesianfmodules:
                o[layer] = cartesianfmodules.process_o(
                    o_input=o[
                        layer - 1],
                    weight_no_row=np.shape(
                        weight[layer])[0],
                    weight_no_col=np.shape(
                        weight[layer])[1],
                    weight=weight[layer],
                    activation_function=self._activation)
            else:
                net[layer] = ohat[layer - 1] * weight[layer]
                if self._activation == 'linear':
                    o[layer] = net[layer]  # linear activation
                elif self._activation == 'tanh':
                    o[layer] = np.tanh(net[layer])  # tanh activation
                elif self._activation == 'sigmoid':
                    # sigmoid activation
                    o[layer] = 1. / (1. + np.exp(-net[layer]))
                ohat[layer] = np.hstack((o[layer], np.matrix([[1.0]])))
        layer += 1  # output layer
        if cartesianfmodules:
            o[layer] = cartesianfmodules.process_o(
                o_input=o[
                    layer - 1],
                weight_no_row=np.shape(
                    weight[layer])[0],
                weight_no_col=np.shape(
                    weight[layer])[1],
                weight=weight[layer],
                activation_function=self._activation)
        else:
            net[layer] = float(ohat[layer - 1] * weight[layer])
            if self._activation == 'linear':
                o[layer] = np.matrix(net[layer])  # linear activation
            elif self._activation == 'tanh':
                o[layer] = np.matrix(np.tanh(net[layer]))  # tanh activation
            elif self._activation == 'sigmoid':
                # sigmoid activation
                o[layer] = np.matrix(1. / (1. + np.exp(-net[layer])))

            del hiddensizes, weight, ohat, net

        E = self._scalings['slope'] * float(o[layer]) + \
            self._scalings['intercept']


        return o, E

##############################################################################
##############################################################################
##############################################################################


class CostFxnandDer:

    """Cost function and its derivative based on squared difference in energy
    and forces,
    to be optimized in setting parameters. Initialize with list of images
    (atoms), a ravel function to decode/encode the vector into parameters,
    and convergence criteria (eV/atom).

        no_of_atoms: number of atoms in image set; note that in CartesianNeural
        number and identity of all atoms in image set should be the same.

        hiddenlayers: structure of hidden layers, e.g., (5, 5)

        ravel: decoder to convert vector to biases/weights and vice versa

        hash_keys: unique keys for each of "images"

        images: list of ASE atoms objects (the training set)

        activation: the type of activation function

        label: name used for all files.

        log: write function at which to log data. Note this must be a
           callable function

        goal: threshold rmse/atom at which simulation is converged

        coordinates_range: range of coordinates of atoms in image set

        train_forces:  boolean to assign whether forces are also trained or not

    """
    #########################################################################

    def __init__(self, no_of_atoms, hiddenlayers, ravel, hash_keys, images,
                 activation, label, log, energy_goal, force_goal, coordinates_range,
                 train_forces, cores, fortran):

        self._no_of_atoms = no_of_atoms
        self._hiddenlayers = hiddenlayers
        self._ravel = ravel
        self._fortran = fortran
        self._hash_keys = hash_keys
        self._energy_goal = energy_goal
        self._force_goal = force_goal
        self._images = images
        self._train_forces = train_forces
        self._activation = activation
        self._label = label
        self._log = log
        self._coordinates_range = coordinates_range
        self._steps = 0
        self._mp = MultiProcess(hash_keys=self._hash_keys,
                                images=self._images,
                                no_procs=cores,
                                fortran=fortran)

        self._mp.ShareImagesBetweenCores()

        self._energy_coefficient = 1.0 # weights for the square error
        self._force_coefficient = 0.04

#        number of all atoms of all images is calculated
        self._no_all_atoms = 0

        self._energy_convergence = False
        self._force_convergence  = False
        
        
        
        for hash_key in self._hash_keys:
            image = self._images[hash_key]
            self._no_all_atoms += len(image)

        if self._fortran is True:

            send_images_data_to_f90_code(self._hash_keys, self._images,
                                         self._hiddenlayers,
                                         self._train_forces,
                                         self._energy_coefficient,
                                         self._force_coefficient,
                                         log,
                                         self._mp._list_sub_images)

    #########################################################################

    def f(self, vector):
        """function to calculate the cost function"""

        log = self._log
        self._weights, self._scalings = self._ravel.to_dicts(vector)

        ff = FeedForward(self._hiddenlayers, self._weights,
                         self._scalings, self._activation)

        if self._fortran is True:
            target_args = (ff,)
            (square_error_energy,
             square_error_forces,
             self._der_parameters_square_error) = \
                self._mp.FeedForwardAndBackpropagation_MP(
                target=cost_fxn_derivatives_wrapper_fortran,
                sub_args=target_args, len_of_parameters=len(vector))
        else:
            target_args = (ff, 
                        self._energy_coefficient, 
                        self._force_coefficient,
                        self._train_forces,
                        self._images)
            (square_error_energy,
             square_error_forces,
             self._der_parameters_square_error) = \
                self._mp.FeedForwardAndBackpropagation_MP(
                target=cost_fxn_derivatives_wrapper_python,
                sub_args=target_args, len_of_parameters=len(vector))

        square_error = self._energy_coefficient * square_error_energy + \
            self._force_coefficient * square_error_forces
        
        self._cost_function = square_error
        self._square_error_forces = square_error_forces

        self._rms_energy_error_per_atom = np.sqrt(square_error_energy / self._no_all_atoms)
        self._rms_force_error_per_atom = np.sqrt(square_error_forces / self._no_all_atoms)
        self._RMSE = np.sqrt(square_error / self._no_all_atoms)
        
        
        # print self._no_all_atoms

        if self._steps == 0:
            if self._train_forces is True:
                head = ('%7s  %25s %15s %19s %15s %25s')
                log(head % ('Step', 'Time', 'Energy Error, eV^2',
                            'Force Error, eV^2', 'rmse-energy, meV/atom', 'rmse-forces, meV/atom'))
                log(head %
                    ('=' * 6, '=' * 24, '=' * 14, '=' * 18, '=' * 14, '=' * 20))
            else:
                head = ('%7s  %25s %19s %15s')
                log(head % ('Step', 'Time', 'Energy Error, eV^2',
                            'RMSE, meV/atom'))
                log(head % ('=' * 6, '=' * 24, '=' * 18, '=' * 14))

        self._steps += 1

        if self._train_forces is True:
            line = ('%7s:' ' %25s' ' %15.2f' ' %19.2f' '%15.2f' '%25.2f')
            log(line % (self._steps - 1,
                        time.ctime(),
                        square_error_energy,
                        square_error_forces,
                        self._rms_energy_error_per_atom * 1000.,
                        self._rms_force_error_per_atom * 1000.))
        else:
            line = ('%7s: %25s %19.2f %15.2f')
            log(line % (self._steps - 1,
                        time.ctime(),
                        square_error,
                        self._RMSE * 1000.))
        
        if self._steps % 100 == 0:
            log('Saving checkpoint data.')
            filename = make_filename(
                self._label,
                'parameters-checkpoint-%i.json' %
                self._steps)
            save_parameters_json(filename, self._no_of_atoms, self._activation,
                                 self._hiddenlayers, self._coordinates_range,
                                 self._ravel, vector)

        if  self._train_forces is True:

            if self._rms_energy_error_per_atom < self._energy_goal and \
            self._energy_convergence is False:
                log('Energy Convergence!')
                self._energy_convergence = True


            if self._rms_force_error_per_atom < self._force_goal and \
            self._force_convergence is False:
                log('Force Convergence!')
                self._force_convergence = True
            
            if self._energy_convergence and self._force_convergence:
                self._weights, self._scalings = self._ravel.to_dicts(vector)
                raise ConvergedException
        else:

            if self._RMSE < self._energy_goal:
                log('Convergence; Force training absent!')
                self._weights, self._scalings = self._ravel.to_dicts(vector)
                raise ConvergedException
                

        gc.collect()

        return self._cost_function

    #########################################################################

    def fprime(self, vector):
        """function to calculate derivative of the cost function"""

        if self._steps == 0:

            self._weights, self._scalings = self._ravel.to_dicts(vector)
            ff = FeedForward(self._hiddenlayers, self._weights, self._scalings,
                             self._activation)
            if self._fortran is True:
                target_args = (ff,)
                (square_error_energy,
                 square_error_forces,
                 self._der_parameters_square_error) = \
                    self._mp.FeedForwardAndBackpropagation_MP(
                    target=cost_fxn_derivatives_wrapper_fortran,
                    sub_args=target_args, len_of_parameters=len(vector))
            else:
                target_args = (ff,
                               self._energy_coefficient,
                               self._force_coefficient,
                               self._train_forces,
                               self._images)
                (square_error_energy,
                 square_error_forces,
                 self._der_parameters_square_error) = \
                    self._mp.FeedForwardAndBackpropagation_MP(
                    target=cost_fxn_derivatives_wrapper_python,
                    sub_args=target_args, len_of_parameters=len(vector))


        self._der_cost_fxn = np.array(self._der_parameters_square_error)

        return self._der_cost_fxn

##############################################################################
##############################################################################
##############################################################################


class ConvergedException(Exception):

    """Kludge to decide when scipy's optimizers are complete."""
    pass

##############################################################################
##############################################################################
##############################################################################


class ParamRavel:

    """Class to ravel and unravel parameter values into a single vector.
    This is used for feeding into the optimizer. Feed in a list of
    dictionaries to initialize. The dictionaries should be one deep.
        weights, scalings: parameters to ravel and unravel
    """

##############################################################################
##############################################################################
##############################################################################

    def __init__(self, weights, scalings):

        count = 0  # Initialize count for number of paramaters.
        self._weightskeys = []
        self._scalingskeys = []

        for key in sorted(weights.keys()):  # layer number starting from 1
            value = weights[key]
            self._weightskeys.append({'key': key,
                                      'shape': np.shape(value),
                                      'size': np.size(value)})
            count += np.size(value)

        for key in sorted(scalings.keys()):  # slope/intercept
            self._scalingskeys.append({'key': key})
            count += 1

        self._vector = np.zeros(count)  # Pre-allocate the vector.

##############################################################################
##############################################################################
##############################################################################

    def to_vector(self, weights, scalings):
        """Puts the weights and scalings dictionaries into a single
        vector and returns it. The bias and weights dictionary need to have
        identical structure to those it was initialized with. This process
        is coined as unraveling."""

        count = 0
        for k in sorted(self._weightskeys):
            lweights = (np.array(weights[k['key']])).ravel()  # This flattens
            self._vector[count: (count + lweights.size)] = lweights
            count += lweights.size

        for k in sorted(self._scalingskeys):
            self._vector[count] = scalings[k['key']]
            count += 1

        return self._vector  # Optimizable 'flattened' vector.

##############################################################################
##############################################################################
##############################################################################

    def to_dicts(self, vector):
        """Puts the vector back into weights and scalings dictionaries of the
        form initialized. Vector must have same length as the output of
        unravel. This process is coined as raveling"""

        assert len(vector) == len(self._vector)  # make sure that the argument
        # Vector has the same length as the attribute vector of the object
        self._vector = vector
        count = 0
        weights = OrderedDict()
        scalings = OrderedDict()
        for k in sorted(self._weightskeys):
            matrix = vector[count:  count + k['size']]
            matrix = matrix.flatten()
            matrix = np.array(matrix.reshape(k['shape']))
            weights[k['key']] = matrix
            count += k['size']
        for k in sorted(self._scalingskeys):
            scalings[k['key']] = vector[count]
            count += 1
        return weights, scalings

##############################################################################
##############################################################################
##############################################################################


class GetForces:

    """
    Class to calculate coordinate derivatives of energy (forces).

    Inputs:
        atoms: ASE atoms object
                   The initial atoms object on which the fingerprints will be
                   generated.
        hiddenlayers: Tuple that conveys the number of hiddenlayers in
                    the NN architecture and eventually the number of nodes
                    in each layer.
                    For example, hiddenlayers = (3,5) means that
                    there exists two hidden layers, the first one with
                    three nodes and the second one having five nodes.
       weights:     Dictionary of arrays of weights connecting one layer to the
                    next. Layers are indexed from 0
                    while the weight dictionary has key values starting from 1.
                    There is one less weight array than there are layers.
                    An example is shown below:
                    weights = {1: np.array([[1,1],[2,4],[4,5],[6,1]]),
                    2: np.array([[4],[6],[1]])}.
                    The arrays are set up
                    to connect node i in the previous layer with node j in the
                    current layer with indices w[i,j]. There are n+1 rows
                    (i values), with the last row being the bias, and m columns
                    (j values). If weights are not given, the arrays will be
                    randomly generated from values between -0.5 and 0.5.
        scalings:   Dictionary of parameters for slope and intercept.
                    They are used in order to remove the final value
                    from the range that the activation function would
                    otherwise be locked in. For example,
                    scalings = {"slope":2, "intercept":1}
        activation: string to assign the type of activation funtion. "linear"
                    refers to linear function, "tanh" refers to tanh function,
                    and "sigmoid" refers to sigmoid function.

    **NOTE: The dimensions of the weight matrix should be consistent with the
            hiddenlayers.**
    """

    ##########################################################################

    def __init__(self, atoms, hiddenlayers, weights, scalings,
                 activation):

        self._atoms = atoms
        self._hiddenlayers = hiddenlayers
        self._weights = weights
        self._scalings = scalings
        self._activation = activation

        if isinstance(hiddenlayers, int):
            self._hiddensizes = [hiddenlayers]
        else:
            self._hiddensizes = [int(part) for part in list(hiddenlayers)]

        self._outputs, NN_energy = self.calculate_outputs()

    ##########################################################################

    def calculate_force(self, mm, ii):
        """ Calculates the force in ii direction on atom index mm"""

        force = 0.

        o = self._outputs
        hiddensizes = self._hiddensizes
        weight = self._weights
        der_o = {}  # node values
        der_o[0] = [0. for i in range(3 * len(self._atoms))]
        der_o[0][3 * mm + ii] = 1.
        layer = 0  # input layer
        for hiddensize in hiddensizes[0:]:
            layer += 1
            temp = np.dot(np.matrix(der_o[layer - 1]),
                          np.delete(weight[layer], -1, 0))
            der_o[layer] = []
            for j in range(np.size(o[layer])):
                if self._activation == 'linear':  # linear function
                    der_o[layer].append(float(temp[0, j]))
                elif self._activation == 'sigmoid':  # sigmoid function
                    der_o[layer].append(
                        float(o[layer][0, j] * (1. - o[layer][0, j])) *
                        float(temp[0, j]))
                elif self._activation == 'tanh':  # tanh function
                    der_o[layer].append(
                        float(1. - o[layer][0, j] * o[layer][0, j]) *
                        float(temp[0, j]))
        layer += 1  # output layer
        temp = np.dot(
            np.matrix(der_o[layer - 1]), np.delete(weight[layer], -1, 0))
        if self._activation == 'linear':  # linear function
            der_o[layer] = float(temp)
        elif self._activation == 'sigmoid':  # sigmoid function
            der_o[layer] = float(o[layer] * (1. - o[layer]) * temp)
        elif self._activation == 'tanh':  # tanh function
            der_o[layer] = float((1. - o[layer] * o[layer]) * temp)
        force = float(-(self._scalings['slope'] * der_o[layer]))

        return force

    #########################################################################

    def calculate_outputs(self):

        ff = FeedForward(self._hiddenlayers, self._weights,
                         self._scalings, self._activation)
        o, NN_energy = ff.get_output(self._atoms)
        return o, NN_energy

##############################################################################


def make_weight_matrices(no_of_atoms, hiddenlayers, activation):
    """Makes random weight matrices from parameters according to the convention
    used in Behler (J. Chem. Physc.: atom-centered symmetry functions for
    constructing ...), but for CartesianNeural.
    """
    if activation == 'linear':
        weight_range = 0.1
    else:
        weight_range = 1.

    W = OrderedDict()
    if isinstance(hiddenlayers, int):
        nn_structure = ([3 * no_of_atoms] + [hiddenlayers] + [1])
    else:
        nn_structure = ([3 * no_of_atoms] +
                        [layer for layer in list(hiddenlayers)] + [1])
    W = OrderedDict()
    W[1] = np.random.random((3 * no_of_atoms + 1,
                             nn_structure[1])) * weight_range - weight_range \
        / 2.
    for layer in range(len(list(nn_structure)) - 3):
        W[layer + 2] = np.random.random(
            (nn_structure[layer + 1] + 1,
             nn_structure[layer + 2])) * weight_range - weight_range / 2.
    W[len(list(nn_structure)) - 1] = np.random.random(
        (nn_structure[-2] + 1, 1)) * weight_range - weight_range / 2.
    for i in range(len(W)):  # biases
        size = W[i + 1][-1].size
        for jj in range(size):
            W[i + 1][-1][jj] = 0.
    return W

##############################################################################


def make_scalings_matrices(images, activation):
    """Makes initial scaling matrices, such that the range of activation
    is scaled to the range of actual energies."""

    max_act_energy = max(image.get_potential_energy(apply_constraint=False)
                         for hash_key, image in images.items())
    min_act_energy = min(image.get_potential_energy(apply_constraint=False)
                         for hash_key, image in images.items())

    scaling = OrderedDict()
    if activation == 'sigmoid':  # sigmoid activation function
        scaling['intercept'] = min_act_energy
        scaling['slope'] = (max_act_energy - min_act_energy)
    elif activation == 'tanh':  # tanh activation function
        scaling['intercept'] = (max_act_energy + min_act_energy) / 2.
        scaling['slope'] = (max_act_energy - min_act_energy) / 2.
    elif activation == 'linear':  # linear activation function
        scaling['intercept'] = (max_act_energy + min_act_energy) / 2.
        scaling['slope'] = (10. ** (-10.)) * \
            (max_act_energy - min_act_energy) / 2.
    return scaling

##############################################################################


def save_parameters_json(filename, no_of_atoms, activation, hiddenlayers,
                         coordinates_range, ravel, vector):
    """Save parameters in json format."""

    parameters = OrderedDict()
    parameters['no_of_atoms'] = no_of_atoms
    parameters['activation'] = activation
    parameters['hiddenlayers'] = hiddenlayers
    parameters['coordinates_range'] = coordinates_range

    weight, scaling = ravel.to_dicts(vector)

    new_weights = OrderedDict()
    for key in sorted(weight.keys()):
        shape = np.shape(weight[key])
        new_weights[key] = []
        for i in range(shape[0]):
            new_weights[key].append([])
            for j in range(shape[1]):
                value = float(weight[key][i, j])
                new_weights[key][i].append(value)

    parameters['weights'] = new_weights
    parameters['scalings'] = scaling

    base_filename = os.path.splitext(filename)[0]
    export_filename = os.path.join(base_filename + '.json')

    with paropen(export_filename, 'wb') as outfile:
        json.dump(parameters, outfile)

##############################################################################


def read_parameters_json(json_file):
    """Reads parameters from JSON file."""

    parameters = json.load(json_file)

    no_of_atoms = parameters['no_of_atoms']
    parameters_weights = parameters['weights']

    new_weights = OrderedDict()
    for key in sorted(parameters_weights.keys()):
        shape = np.shape(parameters_weights[key])
        new_weights[int(key)] = []
        for i in range(shape[0]):
            new_weights[int(key)].append([])
            for j in range(shape[1]):
                value = float(parameters_weights[key][i][j])
                new_weights[int(key)][i].append(value)

    parameters_scalings = parameters['scalings']

    new_scalings = OrderedDict()
    new_scalings['intercept'] = parameters_scalings['intercept']
    new_scalings['slope'] = parameters_scalings['slope']

    weights = new_weights
    scalings = new_scalings
    activation = parameters['activation']
    hiddenlayers = parameters['hiddenlayers']
    coordinates_range = parameters['coordinates_range']

    return (no_of_atoms, activation, hiddenlayers, coordinates_range, weights,
            scalings)

##############################################################################


def make_filename(label, base_filename):
    """Creates a filename from the label and the base_filename which should be
    a string"""

    if not label:
        filename = base_filename
    else:
        filename = os.path.join(label + '-' + base_filename)

    return filename

##############################################################################
##############################################################################
##############################################################################


def make_list_sub_images(hash_keys, images, max_no_queues=None, no_procs=None):
    """ Generates list of sub images.  The total length of the created list is
     no_procs if only no_procs is given, however, if max_no_queue is given then
     the length of sub-images is max_no_queues"""

    if no_procs:

        len_sub_images = int(len(images) / no_procs)
        list_sub_images = []
        for i in range(no_procs - 1):
            sub_images = {}
            for j in range(len_sub_images):
                key_index = 0 + i * len_sub_images + j
                hash = hash_keys[key_index]
                sub_images[hash] = images[hash]
            list_sub_images.append(sub_images)
        last_sub_image = {}

        for i in range(len_sub_images * (no_procs - 1), len(images)):
            key_index = 0 + i
            hash = hash_keys[key_index]
            last_sub_image[hash] = images[hash]
        list_sub_images.append(last_sub_image)
        return list_sub_images

    elif max_no_queues:

        list_sub_images = []
        i = -1
        while i < len(images) / max_no_queues - 1:
            i += 1
            sub_images = {}
            for j in range(max_no_queues):
                key_index = 0 + i * max_no_queues + j
                hash = hash_keys[key_index]
                sub_images[hash] = images[hash]
            list_sub_images.append(sub_images)
        if (i + 1) * max_no_queues < len(images):
            last_sub_image = {}
            for k in range((i + 1) * max_no_queues, len(images)):
                key_index = 0 + k
                hash = hash_keys[key_index]
                last_sub_image[hash] = images[hash]
            list_sub_images.append(last_sub_image)

        return list_sub_images

##############################################################################
##############################################################################
##############################################################################


class MultiProcess:

    """Class to do parallel processing, using multiprocessing package which
    works on Python versions 2.6 and above.
    Inputs:
            hash_keys: unique keys for each of "images"
            images: dictionary of image objects
            no_procs: number of processors
            """

    def __init__(self, hash_keys, images, no_procs, fortran):

        self._hash_keys = hash_keys
        self._fortran = fortran
        self._images = images
        self._no_procs = no_procs
        self._queues = {}
        for x in range(no_procs):
            self._queues[x] = mp.Queue()

    ##########################################################################

    def ShareImagesBetweenCores(self):
        """Derivatives of the cost function with respect to parameters are
        calculated in parallel"""

        self._list_sub_images = make_list_sub_images(hash_keys=self._hash_keys,
                                                     images=self._images,
                                                     no_procs=self._no_procs)

    ##########################################################################

    def FeedForwardAndBackpropagation_MP(self, target, sub_args,
                                         len_of_parameters):
        """Derivatives of the cost function with respect to parameters 
        and the cost function itself are calculated in parallel"""

        args = {}
        for x in range(self._no_procs):
            if self._fortran is True:
                args[x] = (x + 1,) + sub_args + (self._queues[x],)
            else:
                args[x] = (x,) + (self._list_sub_images,) + sub_args + \
                (self._queues[x],)
                # each process gets the necessary arguments for it to start

        square_error_energy = 0.
        square_error_forces = 0.

        der_parameters_square_error = []
        for i in range(len_of_parameters):
            der_parameters_square_error.append(0.)

        processes = [mp.Process(target=target, args=args[x])
                     for x in range(self._no_procs)]

        for x in range(self._no_procs):
            processes[x].start()

        for x in range(self._no_procs):
            processes[x].join()

        sub_square_error_energy = []
        sub_square_error_forces = []
        sub_der_parameters_square_error = []

#       Construct total square_error and derivative with respect to parameters
#       from subprocesses
        for x in range(self._no_procs):
            result = self._queues[x].get()
            sub_square_error_energy.append(result[0])
            sub_square_error_forces.append(result[1])
            sub_der_parameters_square_error.append(result[2])

        for i in range(len(sub_square_error_energy)):
            square_error_energy += sub_square_error_energy[i]
            square_error_forces += sub_square_error_forces[i]
            for j in range(len_of_parameters):
                der_parameters_square_error[j] += \
                    sub_der_parameters_square_error[i][j]

        return (square_error_energy, square_error_forces,
                der_parameters_square_error)

##############################################################################
##############################################################################
##############################################################################


class ExtrapolateError(Exception):

    """Kludge to raise error in the case of extrapolation"""
    pass

##############################################################################
##############################################################################
##############################################################################

def cost_fxn_derivatives_wrapper_fortran(proc_no, ff, queue):
    """wrapper function to be used in multiprocessing for calculating
    cost function and its derivative with respect to parameters in fortran"""


    activation = ff._activation
    weights = ff._weights
    scalings = ff._scalings

    square_error_energy = 0.
    square_error_forces = 0.

    ravel_parameter = ParamRavel(weights, scalings)
    parameters = ravel_parameter.to_vector(weights, scalings)

    print parameters, 'parameters'

    (square_error_energy,
     square_error_forces,
     der_parameters_square_error) =  \
    cartesianfmodules.feedforward_backpropagation(proc=proc_no,
                                             parameters=parameters,
                                             len_of_parameters=len(parameters),
                                             activation=activation)

    queue.put([square_error_energy,
               square_error_forces,
               der_parameters_square_error])

def cost_fxn_derivatives_wrapper_python(proc_no, list_sub_images, ff,
                                 energy_coefficient, force_coefficient,
                                train_forces, images, queue):
    """wrapper function to be used in multiprocessing for calculating
    cost function and its derivative with respect to parameters in python"""

    activation = ff._activation
    weights = ff._weights
    scalings = ff._scalings

    square_error_energy = 0.
    square_error_forces = 0.
 
    der_weights_square_error = OrderedDict() # Pre-allocate arrays
    for j in range(len(weights)):
        der_weights_square_error[j + 1] = \
            np.zeros(shape=np.shape(weights[j + 1]))

    der_scalings_square_error = OrderedDict()
    der_scalings_square_error['intercept'] = 0.
    der_scalings_square_error['slope'] = 0.

    W = {}
    for j in range(len(weights)):
        W[j + 1] = np.delete(weights[j + 1], -1, 0)

    for hash_key in list_sub_images[proc_no].keys():
        atoms = list_sub_images[proc_no][hash_key]
        real_energy = atoms.get_potential_energy(apply_constraint=False)
        real_forces = atoms.get_forces(apply_constraint=False)

        o, NN_energy = ff.get_output(atoms)
        o[0] = np.matrix((atoms.positions).ravel())
        square_error_energy += (NN_energy - real_energy) ** 2. / \
        (len(atoms) ** 2.)

        N = len(o) - 2 # number of hiddenlayers
        D = {}
        for k in range(1, N + 2): # loop through hiddenlayers + output layer
            # calculating D matrix(A diagonal square matrix that helps 
            # incorporate the derivative of the activation functions)
            D[k] = np.zeros(shape=(np.size(o[k]), np.size(o[k])))
            for j in range(np.size(o[k])):
                if activation == 'linear':  # linear
                    D[k][j, j] = 1.
                elif activation == 'sigmoid':  # sigmoid
                    D[k][j, j] = float(o[k][0, j]) * \
                        float((1. - o[k][0, j]))
                elif activation == 'tanh':  # tanh
                    D[k][j, j] = float(1. - o[k][0, j] *
                                       o[k][0, j])
        # Calculating delta
        delta = {} 
        # output layer
        delta[N + 1] = D[N + 1]
        # hidden layers
        for k in range(N, 0, -1): # backpropagate starting from output layer
            delta[k] = np.dot(D[k], np.dot(W[k + 1], delta[k + 1]))


        # Calculating ohat
        ohat = {}
        for k in range(1, N + 2):
            ohat[k - 1] = \
                np.zeros(shape=(1, np.size(o[k - 1]) + 1))
            for j in range(np.size(o[k - 1])):
                ohat[k - 1][0, j] = o[k - 1][0, j]
            ohat[k - 1][0, np.size(o[k - 1])] = 1.0


        der_scalings_square_error['intercept'] += \
            energy_coefficient * 2. * (NN_energy - real_energy) / \
            (len(atoms) ** 2.)
        der_scalings_square_error['slope'] += \
            energy_coefficient * 2. * (NN_energy - real_energy) * float(o[N + 1]) / \
            (len(atoms) ** 2.)
        for k in range(1, N + 2):
            der_weights_square_error[k] += energy_coefficient * 2. * \
                float(NN_energy - real_energy) * \
                float(scalings['slope']) * \
                np.dot(np.matrix(ohat[k - 1]).T, np.matrix(delta[k]).T) / \
                (len(atoms) ** 2.)



        if train_forces is True:

            real_forces = atoms.get_forces(apply_constraint=False)
            nn_forces = np.zeros((len(atoms), 3)) # Pre-allocate
            for self_atom in atoms:
                self_index = self_atom.index
                der_coordinates_o = {}
                der_coordinates_weights_atomic_output = {}
                N = len(o) - 2
                for i in range(3):
                    der_coordinates_o[i] = {}
                    index_der_coords = [0 for p in range(3 * len(atoms))]
                    index_der_coords[3 * self_index + i] = 1.
                    index_der_coords = np.matrix(index_der_coords)
                    der_coordinates_o[i][0] = index_der_coords
                    for k in range(1, N + 2):
                        # Calculating derivative of NN outputs w.r.t.
                        # coordinates
                        temp = np.dot(der_coordinates_o[i][k - 1], W[k])
                        der_coordinates_o[i][k] = []
                        for j in range(np.size(o[k])):
                            # linear function
                            if activation == 'linear':
                                der_coordinates_o[i][k].append(
                                    float(temp[0, j]))
                            # sigmoid function
                            elif activation == 'sigmoid':
                                der_coordinates_o[i][k].append(float(
                                    o[k][0, j] * (1. - o[k][0, j])) *
                                    float(temp[0, j]))
                            elif activation == 'tanh':  # tanh function
                                der_coordinates_o[i][k].append(float(
                                    1. - o[k][0, j] * o[k][0, j]) *
                                    float(temp[0, j]))
                        der_coordinates_o[i][k] = \
                            np.matrix(der_coordinates_o[i][k])
                    der_coordinates_D = {}
                    for k in range(1, N + 2):
                        # Calculating coordinate derivative of D matrix
                        der_coordinates_D[k] = \
                            np.zeros(shape=(np.size(o[k]), np.size(o[k])))
                        for j in range(np.size(o[k])):
                            if activation == 'linear':  # linear
                                der_coordinates_D[k][j, j] = 0.
                            elif activation == 'tanh':  # tanh
                                der_coordinates_D[k][j, j] = \
                                    - 2. * o[k][0, j] * \
                                    der_coordinates_o[i][k][0, j]
                            elif activation == 'sigmoid':  # sigmoid
                                der_coordinates_D[k][j, j] = \
                                    der_coordinates_o[i][k][0, j] \
                                    - 2. * o[k][0, j] * \
                                    der_coordinates_o[i][k][0, j]
                    # Calculating coordinate derivative of delta
                                            # FORTRAN UP_TO_DATE
                    der_coordinates_delta = {}
                    # output layer
                    der_coordinates_delta[N + 1] = der_coordinates_D[N + 1]
                    # hidden layers
                    temp1 = {}
                    temp2 = {}
                    for k in range(N, 0, -1):
                        temp1[k] = np.dot(W[k + 1],
                                          delta[k + 1])
                        temp2[k] = np.dot(W[k + 1],
                                          der_coordinates_delta[k + 1])
                        der_coordinates_delta[k] = \
                            np.dot(der_coordinates_D[k], temp1[k]) + \
                            np.dot(D[k], temp2[k])
                    # Calculating coordinate derivative of ohat and
                    # coordinates weights derivative of atomic_output
                    der_coordinates_ohat = {}
                    der_coordinates_weights_atomic_output[i] = {}
                    for k in range(1, N + 2):
                        der_coordinates_ohat[k - 1] = []
                        for j in range(np.size(der_coordinates_o[i][k - 1])):
                            der_coordinates_ohat[k - 1].append(
                            der_coordinates_o[i][k - 1][0, j])
                        der_coordinates_ohat[k - 1].append(0.)
                        der_coordinates_weights_atomic_output[i][k] = \
                            np.dot(
                            np.matrix(der_coordinates_ohat[k - 1]).T,
                            np.matrix(delta[k]).T) + \
                            np.dot(np.matrix(ohat[k - 1]).T,
                            np.matrix(der_coordinates_delta[k]).T)

                    nn_forces[self_index][i] += - float(scalings['slope']) \
                        * der_coordinates_o[i][N + 1][0]

            

                for i in range(3):
                    square_error_forces += \
                    ((1.0 / 3.0) * (nn_forces[self_index][i] \
                     - real_forces[self_index][i]) ** 2.) / len(atoms)
                    N = len(o) - 2
                    for k in range(1, N + 2):
                        der_weights_square_error[k] += \
                            force_coefficient * (2.0 / 3.0) * \
                            float(scalings['slope']) * \
                            (- nn_forces[self_index][i] +
                             real_forces[self_index][i]) * \
                            der_coordinates_weights_atomic_output[
                            i][k] / len(atoms)
                    der_scalings_square_error['slope'] += \
                        force_coefficient * (2.0 / 3.0) * \
                        der_coordinates_o[i][N + 1][0] * \
                        (- nn_forces[self_index][i] +
                         real_forces[self_index][i]) / len(atoms)



    ravel_der_parameter = ParamRavel(der_weights_square_error,
                                     der_scalings_square_error)
    der_parameters_square_error = \
        ravel_der_parameter.to_vector(der_weights_square_error,
                                      der_scalings_square_error)
        

    queue.put([square_error_energy,
               square_error_forces,
               der_parameters_square_error])

##############################################################################


def compare_train_test_coordinates(atoms, coordinates_range):
    """function to compare train images with the test image and decide whether
    the prediction is interpolation or extrapolation.
    inputs:
        atoms: ASE atom object
        coordinates_range: coordinates range of the train images
    output:
        compare_train_test_coordinates:
        integer: zero for interpolation, and one for extrapolation"""

    compare_train_test_coordinates = 0

    for atom in atoms:
        index = atom.index
        for i in range(3):
            if atoms.positions[index][i] < coordinates_range[0][i] or \
                    atoms.positions[index][i] > coordinates_range[1][i]:
                compare_train_test_coordinates = 1
                break
    return compare_train_test_coordinates

##############################################################################


def interpolate_images(images, json):
    """Function to remove extrapolation images from the "images" set based on
    json data"""

    json_file = paropen(json, 'rb')
    no_of_atoms, activation, hiddenlayers, coordinates_range, weights, \
        scalings = read_parameters_json(json_file)

    if isinstance(images, str):
        extension = os.path.splitext(images)[1]
        if extension == '.traj':
            images = io.Trajectory(images, 'r')
        elif extension == '.db':
            images = io.read(images)

    # Images is converted to dictionary form; key is hash of image.
    dict_images = {}
    for image in images:
        key = hash_image(image)
        dict_images[key] = image
    images = dict_images.copy()
    del dict_images

#   Dictionary of interpolated images set is initialized
    interpolated_images = {}
    for hash_key, image in images.keys():
        atoms = image
        compare_train_test_coordinates = 0
        for atom in atoms:
            index = atom.index
            for i in range(3):
                if atoms.positions[index][i] < coordinates_range[0][i] or \
                        atoms.positions[index][i] > coordinates_range[1][i]:
                    compare_train_test_coordinates = 1
                    break
        if compare_train_test_coordinates == 0:
            interpolated_images[hash_key] = image

    return interpolated_images

##############################################################################


def send_images_data_to_f90_code(hash_keys, images,
                                 hiddenlayers, train_forces,
                                 energy_coefficient, force_coefficient, log,
                                 list_sub_images):
    """Function to send images data to fortran90 code"""

    log('Re-shaping data to send to fortran90...')
    log.tic()

    no_of_images = len(images)
    no_of_atoms_of_images = []

    for hash_key, image in images.items():
        no_of_atoms_of_image = len(image)
        no_of_atoms_of_images.append(no_of_atoms_of_image)


    if isinstance(hiddenlayers, int):
        no_layers_of_nn = 3
    else:
        no_layers_of_nn = len(hiddenlayers) + 2

    if isinstance(hiddenlayers, int):
        nn_structure = ([3 * no_of_atoms_of_image] +
                             [hiddenlayers] + [1])
    else:
        nn_structure = ([3 * no_of_atoms_of_image] +
                             [layer for layer in hiddenlayers] + [1])

    no_nodes_of_layers = list(nn_structure)

    del nn_structure

    len_of_no_nodes_of_layers = len(no_nodes_of_layers)


    count = 0
    for nodes in no_nodes_of_layers:
        count += nodes
    no_all_nodes_of_layers = count

    real_energies = []

    for hash_key in hash_keys:
        atoms = images[hash_key]
        real_energy = atoms.get_potential_energy(apply_constraint=False)
        real_energies.append(real_energy)

    if train_forces is True:
        real_forces = []
        for hash_key in hash_keys:
            atoms = images[hash_key]
            for index in range(len(atoms)):
                real_forces.append(atoms.get_forces \
                    (apply_constraint=False)[index])


    no_procs = len(list_sub_images)

    no_sub_images = []
    for dict_sub_images in list_sub_images:
        no_sub_images.append(len(dict_sub_images))


    input_positions_of_images = []
    for hash_key, image in images.items():
        input_positions_of_images.append(image.positions.ravel())

    log(' ...data re-shaped.', toc=True)

    log('Sending data to fortran90...')

    log.tic()


    cartesianfmodules.images_data.energy_coefficient = energy_coefficient
    cartesianfmodules.images_data.force_coefficient = force_coefficient
    cartesianfmodules.images_data.no_of_images = no_of_images
    cartesianfmodules.images_data.no_layers_of_nn = no_layers_of_nn
    cartesianfmodules.images_data.no_nodes_of_layers = no_nodes_of_layers
    cartesianfmodules.images_data.len_of_no_nodes_of_layers = len_of_no_nodes_of_layers
    cartesianfmodules.images_data.no_all_nodes_of_layers = no_all_nodes_of_layers
    cartesianfmodules.images_data.real_energies = real_energies
    cartesianfmodules.images_data.no_of_atoms_of_images = no_of_atoms_of_images
    cartesianfmodules.images_data.train_forces = train_forces
    cartesianfmodules.images_data.no_procs = no_procs
    cartesianfmodules.images_data.no_sub_images = no_sub_images
    cartesianfmodules.images_data.input_positions_of_images = input_positions_of_images

    print input_positions_of_images, 'input_positions_of_images'



    del real_energies, no_nodes_of_layers, no_sub_images, no_of_atoms_of_images, \
    input_positions_of_images

    if train_forces is True:
        cartesianfmodules.images_data.real_forces = real_forces
        del real_forces

    gc.collect()

    log(' ...data sent to fortran90.', toc=True)


##############################################################################
##############################################################################
##############################################################################